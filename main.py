def factorial(n):
    if n == 0:
        return 1
    return n * factorial(n - 1)


def fibonacci(n):
    if n in [1, 2]:
        return 1
    return fibonacci(n - 1) + fibonacci(n - 2)


names = ['zenx', 'abba', 'molly', 'darren', 'clarissa',
         'xeni', 'henri', 'einstein', 'newton']


class Node:
    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None


class Tree:
    def __init__(self):
        self.root = None

    def alphabetical(self, parent=None):
        if not parent:
            parent = self.root

        if parent.left:
            self.alphabetical(parent.left)
        print('value:', parent.value)
        if parent.right:
            self.alphabetical(parent.right)

    def insert(self, value, parent=None):
        if not parent:
            parent = self.root

        if not self.root:
            self.root = Node(value)
        elif value < parent.value:
            if not parent.left:
                parent.left = Node(value)
            else:
                self.insert(value, parent.left)
        else:
            if not parent.right:
                parent.right = Node(value)
            else:
                self.insert(value, parent.right)


t = Tree()
for name in names:
    t.insert(name)
